![Frozen Fist](https://cdn.drawception.com/images/panels/2017/7-11/dzZzQTj3Hj-4.png)

#**Frozen Fist**
##**Vortex Ops**
>You have just been hired as a summer intern at VortexOps. You've been assigned to work with the software development team, assisting the software architect.The team is currently working on a project codenamed **"Frozen Fist."** They are designing software for semi-autonomous armored assault vehicles. The vehicles are designed to operate either remotely with a human pilot or in "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.
Your job will consist of helping the team with planning, organizing, and designing their latest application project.

Contact: [Marshay McMorris]
(marshay.mcmorris@smail.rasmussen.edu)

#### Pushed to Bitbucket Repository . 18 February, 2018 
### Modified README on BitBucket - 25 February, 2018